<?php
namespace Moogento\SlackCommerce\Block\Adminhtml\System\Config;

use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Model\Queue;

class Notifications extends CustomGroup
{
    protected $_template = 'system/config/notifications.phtml';

    protected $_list = [];

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\View\Helper\Js $jsHelper,
        \Moogento\SlackCommerce\Helper\Config $configHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->_list = [
            [
                'key'  => Queue::KEY_NEW_ORDER,
                'name' => __('New Order'),
            ],
            [
                'key'  => Queue::KEY_NEW_INVOICE,
                'name' => __('New Invoice'),
            ],
            [
                'key'  => Queue::KEY_NEW_SHIPMENT,
                'name' => __('New Shipment'),
            ],
            [
                'key'  => Queue::KEY_NEW_CREDIT,
                'name' => __('New Credit'),
            ],
        ];

        $statuses = $orderConfig->getStatuses();

        foreach ($statuses as $status => $label) {
            $this->_list[] = [
                'key'  => Queue::KEY_NEW_STATUS . '_' . $status,
                'name' => __('New %1 Order', $label),
            ];
        }

        $this->_list[] = [
            'key'  => Queue::KEY_NEW_BACKEND_ACCOUNT,
            'name' => __('New Backend Account'),
        ];

        $this->_list[] = [
            'key'  => Queue::KEY_BACKEND_LOGIN,
            'name' => __('Backend Login'),
        ];

        parent::__construct(
            $context,
            $authSession,
            $jsHelper,
            $configHelper,
            $objectManager,
            $data
        );
    }

    public function getNotificationsJson()
    {
        $notificationsData = [];

        foreach ($this->_list as $data) {
            $key = $data['key'];

            $inherit = $this->showInheritCheckbox();
            if ($inherit) {
                $sendType = $this->_configHelper
                    ->getConfigModel()
                    ->getConfigDataValue(
                        $this->_configHelper->buildPath(
                            $key,
                            Config::SUBTYPE_SEND_TYPE
                        ),
                        $inherit
                    );
            } else {
                $sendType = $this->_configHelper
                    ->getConfigModel()
                    ->getConfigDataValue(
                        $this->_configHelper->buildPath(
                            $key,
                            Config::SUBTYPE_SEND_TYPE
                        )
                    );
            }

            $data['inherit']        = (int) $inherit;
            $data['send_type']      = (string) $sendType;
            $data['custom_channel'] = (string) $this->_configHelper
                ->getConfigModel()
                ->getConfigDataValue(
                    $this->_configHelper->buildPath(
                        $key,
                        Config::SUBTYPE_CUSTOM_CHANNEL
                    )
                );
            $data['colorize']       = (string) $this->_configHelper
                ->getConfigModel()
                ->getConfigDataValue(
                    $this->_configHelper->buildPath(
                        $key,
                        Config::SUBTYPE_COLORIZE
                    )
                );
            $data['color']          = (string) $this->_configHelper
                ->getConfigModel()
                ->getConfigDataValue(
                    $this->_configHelper->buildPath(
                        $key,
                        Config::SUBTYPE_COLOR
                    )
                );

            $notificationsData[] = $data;
        }

        return $this->_jsonHelper->jsonEncode($notificationsData);
    }
}
