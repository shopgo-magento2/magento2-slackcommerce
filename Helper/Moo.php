<?php
namespace Moogento\SlackCommerce\Helper;

use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Module\DbVersionInfo;
use Magento\Framework\Module\Dir;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Module\ResourceInterface;

class Moo extends AbstractHelper
{
    protected $_module = 'Moogento_SlackCommerce';

    protected $_moduleReader;
    protected $_moduleResource;
    protected $_authSession;

    /** @var  \Magento\Framework\Json\Helper\Data */
    protected $_jsonHelper;

    public function __construct(
        Context $context,
        Reader $moduleReader,
        ResourceInterface $moduleResource,
        Session $authSession,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->_moduleReader = $moduleReader;
        $this->_moduleResource = $moduleResource;
        $this->_authSession = $authSession;
        $this->_jsonHelper = $objectManager->get(
            'Magento\Framework\Json\Helper\Data'
        );
        parent::__construct($context, $data);
    }

    public function x()
    {
        // | | | |extensionName|version| | | 
        $d = [
            '',
            '',
            '',
            '',
            'slackcommerce',
            $this->v(),
            '',
            '',
            '',
        ];
        return implode('||', $d);
    }

    //info
    public function i()
    {
        return base64_encode(base64_encode($this->x()));
    }

    //logo
    public function l()
    {
        return base64_encode(
            base64_encode(base64_encode(base64_encode($this->x())))
        );
    }

    public function v()
    {
        $composerFile = $this->_moduleReader->getComposerJsonFiles();

        if ($composerFile->current()) {
            $composerConfig = $this->_jsonHelper->jsonDecode(
                $composerFile->current()
            );
            if ($composerConfig && isset($composerConfig['version'])) {
                return $composerConfig['version'];
            } else {
                return $this->_moduleResource->getDbVersion($this->_module);
            }
        } else {
            return $this->_moduleResource->getDbVersion($this->_module);
        }
    }
}
