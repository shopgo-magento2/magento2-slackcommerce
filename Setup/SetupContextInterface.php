<?php
namespace Moogento\SlackCommerce\Setup;

interface SetupContextInterface
{
    const QUEUE_TABLE = 'moogento_slackcommerce_queue';
    const FAILS_IP_TABLE = 'moogento_slackcommerce_fails_ip';
    const FAILS_TARGET_TABLE = 'moogento_slackcommerce_fails_target';
}
