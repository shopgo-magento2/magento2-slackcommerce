<?php
namespace Moogento\SlackCommerce\Setup;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Module\Dir;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Filesystem;

class UpgradeData implements UpgradeDataInterface
{
    protected $_moduleReader;
    protected $_mediaDirectory;
    protected $_readFactory;

    public function __construct(
        Reader $moduleReader,
        Filesystem $filesystem,
        \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory
    ) {
        $this->_moduleReader = $moduleReader;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(
            DirectoryList::MEDIA
        );
        $this->_readFactory = $readFactory;
    }

    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        if ($setup && $context) {
            if (!$this->_mediaDirectory->isExist(
                'moogento/slack/moogento_logo_small.png'
            )
            ) {
                $this->_mediaDirectory->writeFile(
                    'moogento/slack/moogento_logo_small.png',
                    $this->_readFactory->create(
                        $this->_moduleReader->getModuleDir(
                            Dir::MODULE_VIEW_DIR,
                            'Moogento_SlackCommerce'
                        )
                    )->readFile('moogento_logo_small.png')
                );
            }
        }
    }
}
