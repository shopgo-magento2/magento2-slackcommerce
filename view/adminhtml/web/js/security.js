define([
    'knockout',
    './ko/bind/switch',
    './ko/bind/color'
], function(ko) {
    var SecurityConfig = function(data) {
        var self = this;

        self.immediate_send_type = ko.observable(data.immediate_send_type);
        self.immediate_custom_channel = ko.observable(data.immediate_custom_channel);
        self.immediate_colorize = ko.observable(+data.immediate_colorize);
        self.immediate_color = ko.observable(data.immediate_color);

        self.report_send_type = ko.observable(data.report_send_type);
        self.report_custom_channel = ko.observable(data.report_custom_channel);
        self.report_colorize = ko.observable(+data.report_colorize);
        self.report_color = ko.observable(data.report_color);

        self.hour = ko.observable(data.hour);

        self.total_fails = ko.observable(+data.total_fails);
        self.count_ip_fails = ko.observable(+data.count_ip_fails);
        self.count_target_fails = ko.observable(+data.count_target_fails);
        self.skip_no_fails = ko.observable(+data.skip_no_fails);

        self.buildSecurityName = function(field, multi) {
            return 'groups[security][fields][' + field + '][value]' + (multi ? '[]' : '');
        };
    };

    return function(data, element) {
        ko.applyBindings(new SecurityConfig(data), element);
    };
});
