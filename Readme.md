#Welcome to slackCommerce

slackCommerce is made for Magento 2. It allows you to connect your store to Slack and get instant notification of sales, daily and weekly summaries and security warnings.

More information about the extension can be found here:
> https://moogento.com/slackcommerce

##Installation with Composer

Please follow these instructions to install slackCommerce in your Magento 2 store.

1. Disable the cache with this command:

`bin/magento cache:disable`

2. Add extension to composer require section using this command:
`composer require moogento/magento2-slackcommerce`

3. Enable module and upgrade with these commands:

`bin/magento module:enable --clear-static-content Moogento_SlackCommerce`

`bin/magento setup:upgrade`

4. Check under Stores->Configuration->Advanced->Advanced that you can see `Moogento_SlackCommerce`.  If you see it, then you've successfully installed it!

5. Flush and enable the cache with these commands:

`bin/magento cache:flush`

`bin/magento cache:enable`

6. You should see a Moogento tab in Stores > Configuration. Click this tab and you should see a SlackCommerce section.