<?php
namespace Moogento\SlackCommerce\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Queue extends AbstractDb
{
    protected $_serializableFields
        = [
            'additional_data' => [
                [],
                [],
                false
            ],
        ];

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('moogento_slackcommerce_queue', 'queue_id');
    }
}
