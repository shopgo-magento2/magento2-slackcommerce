<?php
namespace Moogento\SlackCommerce\Model\Notification;

class NewInvoice extends NewOrder
{
    protected $_referenceModel = '\Magento\Sales\Model\Order\Invoice';

    protected function _getOrder()
    {
        return $this->_getReferenceObject()->getOrder();
    }

    protected function _prepareText()
    {
        return __(
            'Invoice #%1 (Order #%2)',
            $this->_getReferenceObject()->getIncrementId(),
            $this->_getOrder()->getIncrementId()
        );
    }

    protected function _getAttachments()
    {
        $orderFields   = $this->_prepareOrderFields();
        $orderAmount   = $this->_prepareOrderAmount();
        $invoiceAmount = $this->_trimZeros(
            $this->_getOrder()->getOrderCurrency()->formatPrecision(
                $this->_getReferenceObject()->getGrandTotal(),
                2,
                [],
                false
            )
        );
        if ($orderAmount['value'] != $invoiceAmount) {
            // Only return Invoice and Order amounts if thy don't match
            return [
                'fields' => array_merge(
                    [
                        [
                            'title' => __('Invoiced Amount'),
                            'value' => $invoiceAmount,
                            'short' => true,
                        ],
                    ],
                    $orderFields
                ),
            ];
        } else {
            return [
                'fields' => $orderFields
            ];
        }
    }
}
