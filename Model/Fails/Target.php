<?php
namespace Moogento\SlackCommerce\Model\Fails;

use Magento\Framework\Model\AbstractModel;

class Target extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Moogento\SlackCommerce\Model\ResourceModel\Fails\Target');
    }
}
