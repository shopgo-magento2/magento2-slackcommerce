<?php
namespace Moogento\SlackCommerce\Observer\SalesOrder;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Model\Queue;
use Moogento\SlackCommerce\Observer\ObserverAbstract;

class InvoiceSaveAfter extends ObserverAbstract
{

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Invoice $invoice */
        $invoice = $observer->getEvent()->getInvoice();

        if ($invoice->getData(Config::NOTIFICATION_FLAG)
            && $this->_configHelper->shouldSend(Queue::KEY_NEW_INVOICE)
        ) {
            $invoice->setData(Config::NOTIFICATION_FLAG, false);

            $queue = $this->_objectManager->create(
                '\Moogento\SlackCommerce\Model\Queue'
            );
            $queue->setData(
                [
                    'event_key' => Queue::KEY_NEW_INVOICE,
                    'reference_id' => $invoice->getId(),
                    'date' => $this->_dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->_logger->warning($e);
            }
        }
    }
}
