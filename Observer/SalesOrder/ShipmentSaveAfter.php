<?php
namespace Moogento\SlackCommerce\Observer\SalesOrder;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Model\Queue;
use Moogento\SlackCommerce\Observer\ObserverAbstract;

class ShipmentSaveAfter extends ObserverAbstract
{

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Shipment $shipment */
        $shipment = $observer->getShipment();

        if ($shipment->getData(Config::NOTIFICATION_FLAG)
            && $this->_configHelper->shouldSend(Queue::KEY_NEW_SHIPMENT)
        ) {
            $shipment->setData(Config::NOTIFICATION_FLAG, false);

            $queue = $this->_objectManager->create(
                '\Moogento\SlackCommerce\Model\Queue'
            );
            $queue->setData(
                [
                    'event_key' => Queue::KEY_NEW_SHIPMENT,
                    'reference_id' => $shipment->getId(),
                    'date' => $this->_dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->_logger->warning($e);
            }
        }
    }
}
