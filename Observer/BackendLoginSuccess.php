<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Model\Queue;

class BackendLoginSuccess extends ObserverAbstract
{

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\User\Model\User $user */
        $user = $observer->getUser();

        if ($this->_configHelper->shouldSend(Queue::KEY_BACKEND_LOGIN)) {
            $queue = $this->_objectManager->create(
                '\Moogento\SlackCommerce\Model\Queue'
            );
            $queue->setData(
                [
                    'event_key' => Queue::KEY_BACKEND_LOGIN,
                    'reference_id' => $user->getId(),
                    'date' => $this->_dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->_logger->warning($e);
            }
        }
    }
}
