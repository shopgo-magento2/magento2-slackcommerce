<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Setup\Model\ObjectManagerProvider;
use Moogento\SlackCommerce\Helper\Config;
use Psr\Log\LoggerInterface;

abstract class ObserverAbstract implements ObserverInterface
{

    /** @var Config */
    protected $_configHelper;

    /** @var ObjectManagerInterface */
    protected $_objectManager;

    /** @var \Magento\Framework\Stdlib\DateTime\DateTime */
    protected $_dateTime;

    /** @var LoggerInterface */
    protected $_logger;

    /** @var \Magento\Framework\DB\Adapter\AdapterInterface */
    protected $_connection;

    /** @var ResourceConnection */
    protected $_resource;

    /** @var \Magento\Framework\App\Request\Http */
    protected $_request;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        ObjectManagerInterface $objectManager,
        LoggerInterface $logger,
        ResourceConnection $resource,
        Config $configHelper
    ) {
        $this->_request = $request;
        $this->_objectManager = $objectManager;
        $this->_configHelper = $configHelper;
        $this->_logger = $logger;
        $this->_resource = $resource;
        $this->_connection = $resource->getConnection();
        $this->_dateTime = $objectManager->get(
            'Magento\Framework\Stdlib\DateTime\DateTime'
        );
    }

    /**
     * @param Observer $observer
     *
     * @return void
     */
    abstract public function execute(Observer $observer);
}
