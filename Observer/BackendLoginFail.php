<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Model\Queue;

class BackendLoginFail extends ObserverAbstract
{

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        $ip        = $this->_configHelper
            ->getRemoteAddress()->getRemoteAddress();
        $urlTarget = $this->_request->getDistroBaseUrl()
                     . $this->_request->getOriginalPathInfo();
        $urlTarget = preg_replace('|/key/([^/]*)|i', '', $urlTarget);
        $urlTarget = str_replace('//', '/', $urlTarget);

        $ipTableName = $this->_resource->getTableName(
            'moogento_slackcommerce_fails_ip'
        );
        $data        = [
            'ip'            => new \Zend_Db_Expr('INET6_ATON("' . $ip . '")'),
            'fails'         => 1,
            'fails_per_day' => 1,
        ];
        $updateData  = [
            'fails'         => new \Zend_Db_Expr('fails+1'),
            'fails_per_day' => new \Zend_Db_Expr('fails_per_day+1'),
        ];
        $this->_connection->insertOnDuplicate($ipTableName, $data, $updateData);

        $targetTableName = $this->_resource->getTableName(
            'moogento_slackcommerce_fails_target'
        );
        $data            = [
            'target'        => $urlTarget,
            'fails_per_day' => 1,
        ];
        $updateData      = [
            'fails_per_day' => new \Zend_Db_Expr('fails_per_day+1'),
        ];
        $this->_connection->insertOnDuplicate(
            $targetTableName,
            $data,
            $updateData
        );

        if ($this->_configHelper->shouldSend(
            Config::KEY_IMMEDIATE,
            Config::SECTION_SECURITY
        )
        ) {
            $message  = $observer->getException()->getMessage();
            $userName = $observer->getUserName();

            $queue = $this->_objectManager->create(
                '\Moogento\SlackCommerce\Model\Queue'
            );
            $queue->setData(
                [
                    'event_key'       => Queue::KEY_BACKEND_LOGIN_FAIL,
                    'reference_id'    => 0,
                    'date'            => $this->_dateTime->gmtDate(
                        "Y-m-d H:i:s"
                    ),
                    'additional_data' => [
                        'username' => $userName,
                        'message'  => $message,
                        'IP'       => $ip,
                        'URL'      => $urlTarget,
                    ],
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->_logger->warning($e);
            }
        }
    }
}
